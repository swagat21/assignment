import React from 'react';
import './App.css';
import {BrowserRouter, Switch, Route} from 'react-router-dom'
import Login from './components/Login';
import Home from './components/Home';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route exact path = "/" component = {Login}/>
          <Route  path = "/home/" component = {Home}/>
          <Route  path = "/logout/" component = {Login}/>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
