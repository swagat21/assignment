import React from "react";
import NavBar from "./NavBar";
import "./../App.css"

class Login extends React.Component {

    constructor() {
        super();
        this.state = {
            errorMessage: "",
            flag: false,
            isHidden: true,
            defaultUser: "abc@infosys.com",
            defaultPassword: "infy123"
        };

        this.handleSubmission = this.handleSubmission.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleSubmission(e) {
        e.preventDefault();
        if (this.state.defaultUser === e.target.elements.userName.value && this.state.defaultPassword === e.target.elements.password.value) {
            this.props.history.push("/home/");
        }
        else {
            this.setState({ flag: true, errorMessage: "Invalid Credentials" })
        }
    }

    handleChange(e) {
        e.preventDefault();
        this.setState({ flag: false })
    }

    render() {
        return (
            <div>
                <NavBar isHidden={this.state.isHidden} />
                <div className="wrapper-login-container">
                    <form id="login-form" onSubmit={this.handleSubmission}>
                        <div className="login-container">
                            <label 
                            className="label"> <b>Username</b> </label>
                            <input type="text" name="userName" onChange={this.handleChange} autoFocus placeholder="xyz@example.com" />
                            <br /><br />
                            <label className="label"><b>Password</b></label>
                            <input type="password" name="password" onChange={this.handleChange} placeholder = "alphanumeric"></input>
                        </div><br />
                        <div className="button">
                            <button type="submit">Log In</button>
                        </div>
                    </form>
                </div><br/>
                <div className="error-message" >{this.state.flag ? this.state.errorMessage : ''}</div>

            </div>

        )
    }

}

export default Login;