import React, { Component } from "react";

class NavBar extends Component {

    constructor(props) {
        super(props);
        this.handleLogout = this.handleLogout.bind(this);
    }

    handleLogout() {
        console.log(this.props)
        this.props.pseudoProps.history.push('/logout/')
    }


    render() {
        return (
            <table className='titlebar'>
                <tbody>
                    <tr>
                        <td>
                            <h3>Assignment App</h3>
                        </td>
                        <td width="1100"></td>
                        <td className="logout-button">
                            <button type="button" onClick={this.handleLogout} hidden={this.props.isHidden}>Logout</button>
                        </td>
                    </tr>
                </tbody>
            </table>
        )
    }
}
export default NavBar;