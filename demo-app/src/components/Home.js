import React, { Component } from "react";
import NavBar from "./NavBar";

class Home extends Component{

    constructor(props){
        super(props);
        this.state = {
            isHidden : false
        }
    }
    render(){
        return(
            <div className = "home-parent">
            <div >
                <NavBar 
                isHidden = {this.state.isHidden}
                pseudoProps = {this.props}/>
            </div>
            <div className = "home-page">
                <h2>Welcome to Home Page</h2>
            </div>
            </div>
        )
    }
}
export default Home;